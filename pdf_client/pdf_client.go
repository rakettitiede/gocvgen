package pdfclient

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net/http"

	"google.golang.org/api/idtoken"
	"rakettitiede.com/gocvgen/config"
)

type PDFClient struct {
	IAPAudience string ""
	ServiceURL  string ""
}

type PDFRequestPayload struct {
	Content string
	Header  string
	Footer  string
}

// GetPDF creates a PDF from given HTML using the PDF service
func (pdfclient PDFClient) GetPDF(contentHTML []byte, headerHTML []byte, footerHTML []byte) bytes.Buffer {
	requestPayload := PDFRequestPayload{
		Content: string(contentHTML),
		Header:  string(headerHTML),
		Footer:  string(footerHTML),
	}

	jsonres, _ := json.Marshal(requestPayload)
	request, err := http.NewRequest(http.MethodPost, pdfclient.ServiceURL, bytes.NewBuffer(jsonres))
	request.Header.Set("Content-Type", "application/json")

	if err != nil {
		panic(err)
	}

	var buf bytes.Buffer
	requestError := pdfclient.makeRequest(&buf, request)

	if requestError != nil {
		panic(requestError)
	}

	return buf
}

func (pdfclient PDFClient) makeRequest(writer io.Writer, request *http.Request) error {
	var client *http.Client
	var err error

	// if running environment is App Engine use the IAP client
	if config.UseIAP() {
		client, err = pdfclient.getIAPClient()

		if err != nil {
			return fmt.Errorf("PDFClient.getIAPClient: %v", err)
		}
	} else {
		client = &http.Client{}
	}

	response, err := client.Do(request)
	if err != nil {
		return fmt.Errorf("client.Do: %v", err)
	}
	defer response.Body.Close()

	if _, err := io.Copy(writer, response.Body); err != nil {
		return fmt.Errorf("io.Copy: %v", err)
	}

	return nil

}

// Creates a client for making requests to services behind Identity Aware Proxy
func (pdfclient PDFClient) getIAPClient() (*http.Client, error) {
	ctx := context.Background()

	// client is a http.Client that automatically adds an "Authorization" header
	// to any requests made.
	client, err := idtoken.NewClient(ctx, pdfclient.IAPAudience)
	if err != nil {
		return client, fmt.Errorf("idtoken.NewClient: %v", err)
	}

	return client, err
}
