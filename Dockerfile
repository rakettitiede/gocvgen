FROM golang:alpine
RUN ["apk" ,"add" ,"--no-cache" ,"git", "build-base"]
WORKDIR /go/src/app
COPY . .
RUN ["go", "get", "-d", "-v"]
RUN ["go", "test"]
RUN ["go", "build", "gocvgen.go"]
ENTRYPOINT [ "./gocvgen" ]
EXPOSE 8080/tcp