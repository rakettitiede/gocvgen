package cli

import (
	"flag"
	"fmt"
	"html/template"
	"io/ioutil"
	"os"

	"rakettitiede.com/gocvgen/models"
	"rakettitiede.com/gocvgen/parser"

	"rakettitiede.com/gocvgen/helpers"

	"github.com/Masterminds/sprig"
	"github.com/skratchdot/open-golang/open"
)

var tpl *template.Template
var cliArgs *models.CliArgs
var portrait []byte

func prepare() {
	if cliArgs.Yaml == "" && flag.Lookup("test.v") == nil {
		fmt.Println("yaml parameter is required!")
		os.Exit(1)
	}

	// These funcs are available inside the template
	fm := template.FuncMap{
		"FormatFromToDate":      helpers.FormatFromToDate,
		"IsHidden":              helpers.IsHidden,
		"FormatUnixToDate":      helpers.FormatUnixToDate,
		"FilterSkills":          helpers.FilterSkills,
		"GradeToString":         helpers.GradeToString,
		"SortSkills":            helpers.SortSkills,
		"SortSkillDescriptions": helpers.SortSkillDescriptions,
	}

	tpl = template.Must(template.New("").Funcs(sprig.FuncMap()).Funcs(fm).ParseGlob(cliArgs.TemplateDir + "/" + cliArgs.TemplateName + "/*.gohtml"))
	if len(cliArgs.PortraitPath) > 0 {
		portrait = helpers.ReadFile(cliArgs.PortraitPath)
	}
}

// Start starts the cli
func Start(args *models.CliArgs) {
	cliArgs = args
	prepare()
	run()
}

func run() {
	yamlParser := parser.YamlParser{
		Tpl:                  tpl,
		Grade:                cliArgs.Grade,
		SalesTeamYaml:        cliArgs.SalesTeamYaml,
		SalesRep:             cliArgs.Sales,
		TemplateDir:          cliArgs.TemplateDir,
		TemplateName:         cliArgs.TemplateName,
		Portrait:             portrait,
		SkillDescriptionYaml: cliArgs.SkillDescriptionYaml,
		IncludeSocialLinks:   cliArgs.SocialMedia,
	}

	yaml := helpers.ReadFile(cliArgs.Yaml)
	generateOutput(yamlParser.GenerateHTMLOutput(yaml))
}

func generateOutput(htmlString []byte) {
	// File will be generated on same folder of yaml file

	// Get exact path from yaml file except file type yaml
	outputPath := cliArgs.Yaml
	outputFile := outputPath + ".html"
	content := htmlString

	// Write to file
	err := ioutil.WriteFile(outputFile, content, 0644)

	if err != nil {
		panic(err)
	}

	browserErr := open.Run(outputFile)

	if browserErr != nil {
		panic(browserErr)
	}
}
