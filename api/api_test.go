package api

import (
	"bytes"
	"fmt"
	"io"
	"mime/multipart"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
	"rakettitiede.com/gocvgen/config"
	"rakettitiede.com/gocvgen/models"
)

var defaultCliArgs *models.CliArgs

func TestMain(m *testing.M) {
	setup()
	code := m.Run()
	os.Exit(code)
}

func setup() {
	os.Setenv(config.ENV_VAR_UI_SERVICE_URL, "*")
	defaultCliArgs = &models.CliArgs{
		TemplateName:         "brainchild",
		TemplateDir:          "../template",
		SalesTeamYaml:        "../template/sales_team.yaml",
		SkillDescriptionYaml: "../template/skill_description.yaml",
	}
}

func TestRootRedirectRoute(t *testing.T) {
	router := setupRouter()
	url := "/"

	w := httptest.NewRecorder()
	req, _ := http.NewRequest(http.MethodGet, url, nil)
	router.ServeHTTP(w, req)

	assert.Equal(t, http.StatusFound, w.Code)
}

func TestPostCVSampleShouldReturnCode200(t *testing.T) {
	router := setupRouter()
	cliArgs = defaultCliArgs
	url := "/cv"
	pathToSampleCV := "../sample_data/innovation_cv_sample.yaml"

	b, mw := createMultipartFormData(t, "file", pathToSampleCV)
	w := httptest.NewRecorder()
	req, _ := http.NewRequest(http.MethodPost, url, &b)
	req.Header.Set("Content-Type", mw.FormDataContentType())
	router.ServeHTTP(w, req)

	assert.Equal(t, 200, w.Code)
}

func TestPostCVEmptyShouldReturnCode400(t *testing.T) {
	router := setupRouter()
	cliArgs = defaultCliArgs
	url := "/cv"

	w := httptest.NewRecorder()
	req, _ := http.NewRequest(http.MethodPost, url, nil)
	router.ServeHTTP(w, req)

	assert.Equal(t, 400, w.Code)
}

func TestLogoutRedirect(t *testing.T) {
	router := setupRouter()
	url := "/logout"

	w := httptest.NewRecorder()
	req, _ := http.NewRequest(http.MethodGet, url, nil)
	router.ServeHTTP(w, req)

	assert.Equal(t, http.StatusFound, w.Code)
}

func createMultipartFormData(t *testing.T, fieldName, fileName string) (bytes.Buffer, *multipart.Writer) {
	var b bytes.Buffer
	var err error
	w := multipart.NewWriter(&b)
	var fw io.Writer
	file := mustOpen(fileName)
	if fw, err = w.CreateFormFile(fieldName, file.Name()); err != nil {
		t.Errorf("Error creating writer: %v", err)
	}
	if _, err = io.Copy(fw, file); err != nil {
		t.Errorf("Error with io.Copy: %v", err)
	}
	w.Close()
	return b, w
}

func mustOpen(f string) *os.File {
	r, err := os.Open(f)
	if err != nil {
		pwd, _ := os.Getwd()
		fmt.Println("PWD: ", pwd)
		panic(err)
	}
	return r
}
