package api

import (
	"html/template"
	"mime/multipart"
	"net/http"

	"github.com/Masterminds/sprig"
	"github.com/gin-gonic/gin"
	"rakettitiede.com/gocvgen/api/middleware"
	"rakettitiede.com/gocvgen/config"
	"rakettitiede.com/gocvgen/helpers"
	"rakettitiede.com/gocvgen/models"
	"rakettitiede.com/gocvgen/parser"
)

var tpl *template.Template
var cliArgs *models.CliArgs
var fm template.FuncMap = template.FuncMap{
	"FormatFromToDate":      helpers.FormatFromToDate,
	"IsHidden":              helpers.IsHidden,
	"FormatUnixToDate":      helpers.FormatUnixToDate,
	"FilterSkills":          helpers.FilterSkills,
	"GradeToString":         helpers.GradeToString,
	"SortSkills":            helpers.SortSkills,
	"SortSkillDescriptions": helpers.SortSkillDescriptions,
}

// Start starts the api server
func Start(args *models.CliArgs) {
	cliArgs = args
	run()
}

func run() *gin.Engine {
	r := setupRouter()

	port := config.GetEnvVar(config.ENV_VAR_SERVER_PORT)
	if port == "" {
		port = "8080"
	}

	r.Run(":" + port)

	return r
}

func setupRouter() *gin.Engine {
	// default engine includes Logger and Recovery middleware
	r := gin.Default()
	r.Use(middleware.CORS())

	if config.UseIAP() {
		// use IAP token validation only in App Engine environment
		r.Use(middleware.IAP())
	}

	// basic groupings
	r.GET("/", defaultHandler)

	// resources for testing purposes as described in https://cloud.google.com/iap/docs/special-urls-and-headers-howto#testing_jwt_verification
	r.GET("/_gcp_iap/secure_token_test/:operation", iapTestTokenHandler)

	// CV resource
	cvResource := r.Group("/cv")
	cvResource.POST("", postCVHandler)
	cvResource.GET("/template", getCVTemplateHandler)

	r.GET("/logout", logoutHandler)

	return r
}

func iapTestTokenHandler(c *gin.Context) {
	c.String(http.StatusOK, c.Param("operation"))
}

func defaultHandler(c *gin.Context) {
	c.Redirect(http.StatusFound, "/ui/")
}

func getCVTemplateHandler(c *gin.Context) {
	sampleCV := helpers.ReadFile("./sample_data/innovation_cv_sample.yaml")
	c.Data(http.StatusOK, "text/yaml", sampleCV)
}

func postCVHandler(c *gin.Context) {
	// set default values for params
	cvPostData := models.CVPostData{
		SkillGradeThreshold: cliArgs.Grade,
		ResultFormat:        HTML.String(),
		TemplateName:        cliArgs.TemplateName,
		IncludeSales:        false,
		IncludeSocialLinks:  cliArgs.SocialMedia,
	}

	// try to bind request data, report 400 Bad request if binding fails
	if err := c.ShouldBind(&cvPostData); err != nil {
		vError := models.ValidationError{
			Status:  http.StatusBadRequest,
			Message: err.Error(),
		}
		c.AbortWithStatusJSON(http.StatusBadRequest, vError)
		return
	}

	tpl = template.Must(template.New("").Funcs(sprig.FuncMap()).Funcs(fm).ParseGlob(cliArgs.TemplateDir + "/" + cvPostData.TemplateName + "/*.gohtml"))
	portrait := readOptionalFormFile(cvPostData.Portrait)
	yaml := readFormFile(cvPostData.Yaml)

	if err := validateResume(yaml); err != nil {
		vError := models.ValidationError{
			Status:  http.StatusBadRequest,
			Message: err.Error(),
		}
		c.AbortWithStatusJSON(http.StatusBadRequest, vError)
		return
	}

	var salesRep string
	if cvPostData.IncludeSales {
		salesRep = cliArgs.Sales
	}

	yamlParser := parser.YamlParser{
		Tpl:                  tpl,
		Grade:                cvPostData.SkillGradeThreshold,
		SalesTeamYaml:        cliArgs.SalesTeamYaml,
		SalesRep:             salesRep,
		IncludeSocialLinks:   cvPostData.IncludeSocialLinks,
		TemplateDir:          cliArgs.TemplateDir,
		TemplateName:         cvPostData.TemplateName,
		Portrait:             portrait,
		SkillDescriptionYaml: cliArgs.SkillDescriptionYaml,
	}

	if cvPostData.ResultFormat != PDF.String() {
		generatedContent := yamlParser.GenerateHTMLOutput(yaml)
		bytes := []byte(generatedContent)
		c.Data(http.StatusOK, "text/html; charset=utf-8", bytes)
	} else {
		generatedContent := yamlParser.GeneratePDFOutputHeadless(yaml)
		c.Data(http.StatusOK, "application/pdf", generatedContent)
	}
}

func logoutHandler(c *gin.Context) {
	// redirect to special url to revoke auth cookies set by IAP: https://cloud.google.com/iap/docs/special-urls-and-headers-howto
	c.Redirect(http.StatusFound, "/_gcp_iap/clear_login_cookie")
}

//func readFormFile(c *gin.Context, name string) []byte {
func readFormFile(file *multipart.FileHeader) []byte {
	// open file
	src, fileOpenErr := file.Open()
	defer src.Close()

	if fileOpenErr != nil {
		panic(fileOpenErr)
	}

	// read file to byte array
	buf := make([]byte, file.Size)
	_, fileReadErr := src.Read(buf)

	if fileReadErr != nil {
		panic(fileReadErr)
	}

	return buf
}

func readOptionalFormFile(file *multipart.FileHeader) []byte {
	if file != nil {
		return readFormFile(file)
	}

	return []byte{}
}

func validateResume(bs []byte) error {
	_, err := parser.ParseResume(bs)

	return err
}

type ResultDocumentType int

const (
	HTML ResultDocumentType = iota
	PDF
)

func (rdt ResultDocumentType) String() string {
	return [...]string{"html", "pdf"}[rdt]
}
