package middleware

import (
	"github.com/gin-gonic/gin"
	"rakettitiede.com/gocvgen/config"

	"github.com/gin-contrib/cors"
)

// CORS middleware applies cors rules for service
func CORS() gin.HandlerFunc {
	corsConfig := cors.DefaultConfig()
	corsConfig.AllowOrigins = []string{config.GetEnvVar(config.ENV_VAR_UI_SERVICE_URL)}
	corsConfig.AllowHeaders = []string{"Origin"}
	corsConfig.AllowCredentials = true

	return cors.New(corsConfig)
}
