package middleware

import (
	"bytes"
	"context"
	"fmt"
	"io"
	"net/http"

	"github.com/gin-gonic/gin"
	"google.golang.org/api/idtoken"
	"rakettitiede.com/gocvgen/config"
)

// IAP middleware validates the token set by Google Identity Aware Proxy for App Engine
func IAP() gin.HandlerFunc {
	return func(c *gin.Context) {
		iapJWT := c.Request.Header.Get("X-Goog-IAP-JWT-Assertion")
		projectNumber := config.GetEnvVar(config.ENV_VAR_GCP_PROJECT_NUMBER)
		projectID := config.GetEnvVar(config.ENV_VAR_GCP_PROJECT_ID)
		var buf bytes.Buffer

		err := validateJWTFromAppEngine(&buf, iapJWT, projectNumber, projectID)

		if err != nil {
			c.AbortWithStatus(http.StatusUnauthorized)
		}

		fmt.Println(buf.String())
	}
}

func validateJWTFromAppEngine(w io.Writer, iapJWT, projectNumber, projectID string) error {
	ctx := context.Background()
	aud := fmt.Sprintf("/projects/%s/apps/%s", projectNumber, projectID)

	payload, err := idtoken.Validate(ctx, iapJWT, aud)
	if err != nil {
		return fmt.Errorf("idtoken.Validate: %v", err)
	}

	// payload contains the JWT claims for further inspection or validation
	fmt.Fprintf(w, "payload: %v", payload)

	return nil
}
