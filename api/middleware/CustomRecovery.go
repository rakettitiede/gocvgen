package middleware

import (
	"io"
	"log"
	"net/http/httputil"

	"github.com/gin-gonic/gin"
	"github.com/go-errors/errors"
)

/* Custom recovery handler for gin, not currently in use */
func Recovery(f func(c *gin.Context, err interface{})) gin.HandlerFunc {
	return RecoveryWithWriter(f, gin.DefaultErrorWriter)
}

func RecoveryWithWriter(f func(c *gin.Context, err interface{}), out io.Writer) gin.HandlerFunc {
	var logger *log.Logger
	if out != nil {
		logger = log.New(out, "\n\n\x1b[31m", log.LstdFlags)
	}

	return func(c *gin.Context) {
		defer func() {
			if err := recover(); err != nil {
				if logger != nil {
					httprequest, _ := httputil.DumpRequest(c.Request, false)
					goErr := errors.Wrap(err, 0)
					reset := string([]byte{27, 91, 48, 109})
					logger.Printf("[Nice Recovery] panic recovered:\n\n%s%s\n\n%s%s", httprequest, goErr.Error(), goErr.Stack(), reset)
				}

				f(c, err)
			}
		}()
		c.Next() // execute all the handlers
	}
}

func recoveryHandler(c *gin.Context, err interface{}) {
	c.JSON(500, gin.H{
		"title": "Error",
		"err":   errors.Wrap(err, 0).Error(),
	})
}
