package config

import (
	"os"
	"testing"
)

func TestIsAppEngineEnv(t *testing.T) {
	envParamName := ENV_VAR_GAE_APPLICATION
	os.Setenv(envParamName, "app engine application name")

	if !IsAppEngineEnv() {
		t.Errorf("App Engine environment not correctly detected with env param %v : %v", envParamName, GetEnvVar(envParamName))
	}
}
