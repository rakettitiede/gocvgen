package config

import (
	"os"
	"strconv"
)

const (
	ENV_VAR_CVGEN_ENV          = "CVGEN_ENV"
	ENV_VAR_UI_SERVICE_URL     = "UI_SERVICE_URL"
	ENV_VAR_SERVER_PORT        = "PORT"
	ENV_VAR_PDF_SERVICE_URL    = "PDF_SERVICE_URL"
	ENV_VAR_IAP_AUDIENCE       = "IAP_AUDIENCE"
	ENV_VAR_GCP_PROJECT_NUMBER = "GCP_PROJECT_NUMBER"
	ENV_VAR_GCP_PROJECT_ID     = "GCP_PROJECT_ID"
	ENV_VAR_GAE_APPLICATION    = "GAE_APPLICATION"
	ENV_VAR_USE_IAP 		   = "USE_IAP"
)

// GetEnvVar retrieves the environment variable with given name
func GetEnvVar(name string) string {
	return os.Getenv(name)
}

// IsAppEngineEnv checks if current running environment is App Engine environment
func IsAppEngineEnv() bool {
	_, present := os.LookupEnv(ENV_VAR_GAE_APPLICATION)
	return present

}

func UseIAP() bool {
	value := GetEnvVar(ENV_VAR_USE_IAP)
	converted, _ := strconv.ParseBool(value)

	return converted
}
