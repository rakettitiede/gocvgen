package helpers

import (
	"testing"

	"rakettitiede.com/gocvgen/models"
)

func TestIsHidden(t *testing.T) {
	notHidden1 := "Client A"
	notHidden2 := "Client B!"
	hidden := "Client C !"

	if IsHidden(notHidden1) {
		t.Errorf("Expected %s not to be hidden", notHidden1)
	}

	if IsHidden(notHidden2) {
		t.Errorf("Expected %s not to be hidden", notHidden2)
	}

	if !IsHidden(hidden) {
		t.Errorf("Expected %s to be hidden", hidden)
	}
}

func TestFilterSkills(t *testing.T) {
	skillsMap := make(models.SkillsMap)

	aSkills := make(map[string]int)
	bSkills := make(map[string]int)

	aSkills["a1"] = 1
	aSkills["b1"] = 2
	bSkills["a2"] = 1
	bSkills["b3"] = 2
	bSkills["c3"] = 3

	skillsMap["a"] = aSkills
	skillsMap["b"] = bSkills

	filtered := FilterSkills(skillsMap, 2)

	if len(filtered["a"]) != 1 {
		t.Errorf("Invalid amount of skills after filtering. Expected %v, got %v", 1, len(filtered["a"]))
	}

	if _, ok := filtered["a"]["a1"]; ok {
		t.Errorf("Map contains a value that should have deen filtered: %v", filtered["a"]["a1"])
	}

	if len(filtered["b"]) != 2 {
		t.Errorf("Invalid amount of skills after filtering. Expected %v, got %v", 2, len(filtered["a"]))
	}

	if _, ok := filtered["b"]["a2"]; ok {
		t.Errorf("Map contains a value that should have deen filtered: %v", filtered["b"]["a2"])
	}
}

func TestSortSkills(t *testing.T) {
	skillsMap := make(map[string]map[string]int)

	aSkills := make(map[string]int)
	bSkills := make(map[string]int)

	aSkills["a1"] = 1
	aSkills["b1"] = 2
	bSkills["a2"] = 1
	bSkills["b3"] = 2
	bSkills["c3"] = 3

	skillsMap["a"] = aSkills
	skillsMap["b"] = bSkills

	skillCategories := SortSkills(skillsMap)

	// the number of categories should match the number of given skill categories
	if len(skillCategories) != len(skillsMap) {
		t.Errorf("Invalid amount of skill categories. Expected %v, got %v", len(skillsMap), len(skillCategories))
	}

	// Each category should have a lenght of skills
	for _, skillCategory := range skillCategories {
		if len(skillsMap[skillCategory.Category]) != len(skillCategory.Skills) {
			t.Errorf("Invalid amount of Skills. Expected %v, got %v", len(skillsMap[skillCategory.Category]), len(skillCategory.Skills))
		}
	}

}

func TestSortSkillDescriptions(t *testing.T) {
	skillsDescriptions := make([]models.SkillDescription, 0)
	skillsDescriptions = append(skillsDescriptions, models.SkillDescription{Grade: 1, Description: "a"})
	skillsDescriptions = append(skillsDescriptions, models.SkillDescription{Grade: 3, Description: "c"})
	skillsDescriptions = append(skillsDescriptions, models.SkillDescription{Grade: 2, Description: "b"})

	sortedSkillsDescriptions := SortSkillDescriptions(skillsDescriptions)

	var result string = ""
	for _, skillDescription := range sortedSkillsDescriptions {
		result = result + skillDescription.Description
	}

	if result != "cba" {
		t.Errorf("Invalid sort order. Expected %v, got %v", "cba", result)
	}
}
