package helpers

import (
	"bufio"
	"encoding/base64"
	"fmt"
	"io/ioutil"
	"os"
	"sort"
	"strings"
	"time"

	"rakettitiede.com/gocvgen/models"
)

func FormatUnixToDate(u int64) string {
	t := time.Unix(u, 0)

	return t.Format("2006-01-02")
}

func FormatFromToDate(from string, to string) string {
	return fmt.Sprintf("%s - %s", from, to)
}

func IsHidden(s string) bool {
	return strings.HasSuffix(s, " !")
}

func GradeToString(grade int) string {
	if grade <= 2 {
		return "Basic"
	} else if grade == 3 {
		return "Proficient"
	} else if grade <= 5 {
		return "Master"
	}
	return "Invalid"
}

func FilterSkills(m models.SkillsMap, gradeLimit int) models.SkillsMap {
	for i, category := range m {
		for name, grade := range category {
			if grade < gradeLimit {
				delete(category, name)
			}
		}

		// remove categories with empty skills
		if len(category) == 0 {
			delete(m, i)
		}
	}

	return m
}

func SortSkills(skillMap models.SkillsMap) []models.SkillCategory {
	var skillCategoryNames []string

	for skillCategory := range skillMap {
		skillCategoryNames = append(skillCategoryNames, skillCategory)
	}

	sort.Strings(skillCategoryNames)
	sortedSkillCategories := make([]models.SkillCategory, len(skillCategoryNames))
	skillCategoryIndex := 0
	for _, skillCategory := range skillCategoryNames {
		skills := skillMap[skillCategory]

		var skillNames []string

		for skillName := range skills {
			skillNames = append(skillNames, skillName)
		}

		sort.Strings(skillNames)
		sortedSkills := make([]models.Skill, len(skillNames))
		skillIndex := 0
		for _, skillName := range skillNames {
			grade := skills[skillName]
			sortedSkills[skillIndex] = models.Skill{skillName, grade}
			skillIndex++
		}
		sortedSkillCategories[skillCategoryIndex] = models.SkillCategory{skillCategory, sortedSkills}
		skillCategoryIndex++
	}

	return sortedSkillCategories
}

// SortSkillDescriptions sorts an array of SkillDescriptions in descending order by grade
func SortSkillDescriptions(skillDescriptions []models.SkillDescription) []models.SkillDescription {
	sort.Slice(skillDescriptions, func(i, j int) bool {
		return skillDescriptions[i].Grade > skillDescriptions[j].Grade
	})

	return skillDescriptions
}

func ReadFile(fileName string) []byte {
	bs, err := ioutil.ReadFile(fileName)

	if err != nil {
		fmt.Println(err)
		panic(err)
	}

	return bs
}

func Base64EncodeFile(fileName string) string {
	f, fErr := os.Open(fileName)
	defer f.Close()

	if fErr != nil {
		fmt.Println(fErr)
		panic(fErr)
	}

	r := bufio.NewReader(f)
	data, rErr := ioutil.ReadAll(r)

	if rErr != nil {
		fmt.Println(rErr)
		panic(rErr)
	}

	return Base64Encode(data)
}

func Base64Encode(data []byte) string {
	return base64.StdEncoding.EncodeToString(data)
}
