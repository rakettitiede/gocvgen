package models

import "mime/multipart"

type CVPostData struct {
	SkillGradeThreshold int                   `form:"skillGradeThreshold" binding:"omitempty"`
	IncludeSales        bool                  `form:"includeSales" binding:"omitempty"`
	IncludeSocialLinks  bool                  `form:"includeSocialLinks" binding:"omitempty"`
	ResultFormat        string                `form:"resultFormat" binding:"omitempty"`
	TemplateName        string                `form:"templateName" binding:"omitempty"`
	Yaml                *multipart.FileHeader `form:"file" binding:"required"`
	Portrait            *multipart.FileHeader `form:"portrait" binding:"omitempty"`
}
