package models

type ValidationError struct {
	Status  int    `json:"status"`
	Message string `json:"message"`
}
