package models

type CliArgs struct {
	TemplateDir          string
	SalesTeamYaml        string
	Sales                string
	Yaml                 string
	Grade                int
	Server               bool
	TemplateName         string
	PortraitPath         string
	SkillDescriptionYaml string
	SocialMedia          bool
}
