package models

import "html/template"

// Social
type Social struct {
	Linkedin string `yaml:"linkedin,omitempty"`
	Blog     string `yaml:"blog,omitempty"`
	Github   string `yaml:"github,omitempty"`
}

// OtherPersonal
type Basics struct {
	Email       string `yaml:"email,omitempty"`
	DateOfBirth string `yaml:"date_of_birth,omitempty"`
	Nationality string `yaml:"nationality,omitempty"`
	Location    string `yaml:"location,omitempty"`
}

// Education
type Education struct {
	Institute   string `yaml:"institute"`
	Degree      string `yaml:"degree,omitempty"`
	Year        int    `yaml:"year,omitempty"`
	Description string `yaml:"description,omitempty"`
}

// Volunteering
type Volunteering struct {
	Responsibility string `yaml:"responsibility,omitempty"`
	Location       string `yaml:"location,omitempty"`
	Year           int    `yaml:"year,omitempty"`
}

// Conferences
type Conferences struct {
	Event string `yaml:"event,omitempty"`
	City  string `yaml:"city,omitempty"`
	Year  int    `yaml:"year,omitempty"`
}

// Certification
type Certification struct {
	Organizer   string `yaml:"organizer,omitempty"`
	Description string `yaml:"description"`
	Year        int    `yaml:"year,omitempty"`
}

type Quotation struct {
	Quote    string          `yaml:"quote"`
	Customer QuotingCustomer `yaml:"customer"`
}

type QuotingCustomer struct {
	Company string `yaml:"company"`
	Name    string `yaml:"name"`
	Role    string `yaml:"role"`
}

// Personal
type Personal struct {
	Title        string         `yaml:"title,omitempty"`
	FirstName    string         `yaml:"first_name,omitempty"`
	LastName     string         `yaml:"last_name,omitempty"`
	ImagePath    string         `yaml:"image,omitempty"`
	Email        string         `yaml:"email,omitempty"`
	DateOfBirth  string         `yaml:"date_of_birth,omitempty"`
	Nationality  string         `yaml:"nationality,omitempty"`
	Location     string         `yaml:"location,omitempty"`
	Languages    string         `yaml:"languages,omitempty"`
	Pitch        string         `yaml:"pitch,omitempty"`
	Description  string         `yaml:"description,omitempty"`
	Social       *Social        `yaml:"social,omitempty"`
	Basics       *Basics        `yaml:"basics,omitempty"`
	Educations   []Education    `yaml:"education,omitempty"`
	Interests    []string       `yaml:"interests,omitempty"`
	Competences  []string       `yaml:"competences,omitempty"`
	Volunteering []Volunteering `yaml:"volunteering,omitempty"`
	Conferences  []Conferences  `yaml:"conferences,omitempty"`
}

type Project struct {
	Customer     string   `yaml:"customer,omitempty"`
	Role         string   `yaml:"role,omitempty"`
	From         string   `yaml:"from"`
	To           string   `yaml:"to,omitempty"`
	ProjectTitle string   `yaml:"project_title,omitempty"`
	Technologies string   `yaml:"technologies,omitempty"`
	Descriptions []string `yaml:"description,omitempty"`
}

// Experience
type Experience struct {
	Company      string    `yaml:"company"`
	Title        string    `yaml:"title"`
	From         string    `yaml:"from"`
	To           string    `yaml:"to,omitempty"`
	Projects     []Project `yaml:"projects,omitempty"`
	Descriptions []string  `yaml:"description,omitempty"`
}

// Metadata
type Meta struct {
	CreationTime       int64
	ExpirationTime     int64
	GradeLimit         int
	IncludeSocialLinks bool
	Logo               string
	Portrait           string
	CSS                template.CSS
}

// Type alias for skills
type SkillsMap map[string]map[string]int

type SkillCategory struct {
	Category string
	Skills   []Skill
}

type OrganizationalSkill string

// Resume
type Resume struct {
	Personal          		`yaml:"personal"`
	Skills            		SkillsMap `yaml:"skills"`
	SkillDescriptions 		[]SkillDescription
	Experiences       		[]Experience    `yaml:"experience"`
	Certifications    		[]Certification `yaml:"certifications,omitempty"`
	Quotations        		[]Quotation     `yaml:"quotations,omitempty"`
	Meta
	SalesContact
	OrganizationalSkills	[]string	`yaml:"organizational_skills,omitempty"`
}

// SalesTeam contact
type SalesContact struct {
	Name  string
	Phone string
	Email string
}

type SalesTeam map[string]SalesContact

type Skill struct {
	Name  string
	Grade int
}

type SkillDescription struct {
	Grade       int    `yaml:grade`
	Description string `yaml:description`
}

// Document
type Document struct {
	Resume `yaml:"resume"`
}

// Footer
type Footer struct {
	Logo string
}
