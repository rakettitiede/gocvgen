package main

import (
	"flag"

	"github.com/joho/godotenv"

	"rakettitiede.com/gocvgen/config"

	"rakettitiede.com/gocvgen/api"
	"rakettitiede.com/gocvgen/cli"
	"rakettitiede.com/gocvgen/models"
)

var cliArguments = &models.CliArgs{}

const (
	defaultTemplateDir          = "./template"
	templateDirUsage            = "Directory where gohtml templates are"
	defaultSalesTeamYaml        = "./template/sales_team.yaml"
	salesTeamYamlUsage          = "Location of Sales team YAML file"
	defaultSales                = "iida"
	salesUsage                  = "Sales contact for Resume (iida || marietta)"
	defaultYaml                 = ""
	yamlUsage                   = "Location of Innovation CV YAML file, REQUIRED"
	defaultGrade                = 3
	gradeUsage                  = "Do not include skills below this grade"
	defaultServer               = false
	serverUsage                 = "Start the API server"
	defaultTemplateName         = "brainchild"
	templateNameUsage           = "Name of template used to create a CV (innovation || brainchild)"
	defaultPortraitPath         = ""
	portraitPathUsage           = "Path to your portrait image"
	defaultSkillDescriptionYaml = "./template/skill_description.yaml"
	skillDescriptionYamlUsage   = "Location of skill description YAML file"
	socialMediaUsage            = "Include social media links"
	defaultSocialMedia          = true
)

func init() {
	flag.StringVar(&cliArguments.TemplateDir, "template_dir", defaultTemplateDir, templateDirUsage)
	flag.StringVar(&cliArguments.SalesTeamYaml, "sales_team_yaml", defaultSalesTeamYaml, salesTeamYamlUsage)
	flag.StringVar(&cliArguments.Sales, "sales", defaultSales, salesUsage)
	flag.StringVar(&cliArguments.Yaml, "yaml", defaultYaml, yamlUsage)
	flag.IntVar(&cliArguments.Grade, "grade", defaultGrade, gradeUsage)
	flag.BoolVar(&cliArguments.Server, "server", getServerDefault(), serverUsage)
	flag.StringVar(&cliArguments.TemplateName, "template", defaultTemplateName, templateNameUsage)
	flag.StringVar(&cliArguments.PortraitPath, "portrait", defaultPortraitPath, portraitPathUsage)
	flag.StringVar(&cliArguments.SkillDescriptionYaml, "skill_description_yaml", defaultSkillDescriptionYaml, skillDescriptionYamlUsage)
	flag.BoolVar(&cliArguments.SocialMedia, "socialmedia", defaultSocialMedia, socialMediaUsage)

	flag.Parse()

	// load environment params from dotenv config
	loadEnv(config.ENV_VAR_CVGEN_ENV)
}

func main() {
	if cliArguments.Server {
		api.Start(cliArguments)

	} else {
		cli.Start(cliArguments)
	}
}

// server should always be started when deployed to app engine
func getServerDefault() bool {
	if config.IsAppEngineEnv() {
		return true
	}

	return defaultServer
}

func loadEnv(envParamName string) {
	env := config.GetEnvVar(envParamName)
	if "" == env {
		env = "development"
	}

	godotenv.Load(".env." + env + ".local")
	if "test" != env {
		godotenv.Load(".env.local")
	}
	godotenv.Load(".env." + env)
	godotenv.Load()
}
