# GOCVGEN

## Notice!

This project is in testing phase where structure of YAML formatted CV is being evaluated.

## Overview

A commandline tool to render HMTL CV based on yaml document.

Tool is build using Go programming language and uses Go standard `html/template` package.

## Build & install

### Build

1. Instal golang environment
1. Go to project root directory
1. Build executable: `go build gocvgen.go`

## Usage

After successful compilation you should have binary named `gocvgen`(`gocvgen.exe` in windows)

Commandline arguments: `./gocvgen --help`

```
Usage of ./gocvgen:
  -grade int
    	Do not include skills below this grade (default 3)
  -portrait string
    	Path to your portrait image
  -sales string
    	Sales contact for Resume (iida || marietta) (default "iida")
  -sales_team_yaml string
    	Location of Sales team YAML file (default "./template/sales_team.yaml")
  -server
    	Start the API server
  -skill_description_yaml string
    	Location of skill description YAML file (default "./template/skill_description.yaml")
  -socialmedia
    	Include social media links (default true)
  -template string
    	Name of template used to create a CV (innovation || brainchild) (default "innovation")
  -template_dir string
    	Directory where gohtml templates are (default "./template")
  -yaml string
    	Location of Innovation CV YAML file, REQUIRED
```

After successfully execute command above, new HTML file will be generated at the same directory of yaml file. New tab will be opened in default browser

Use Print function from browser to generate PDF

### Examples

Generate a CV from included sample to stdout:

(In gocvgen directory)

```
./gocvgen -grade 3 -sales iida -yaml sample_data/personal_full_example.yaml
```

### Testing

Executed standard go test runner

```
go test
```

or if you want to run tests in all packages

```
go test ./...
```

### Adding new templates

You can add a new CV template by creating a new folder under the template/ folder (the name of the folder will be the name of the template). The entry point of your template has to be named main.gohtml and the CSS styles have to be placed in styles/css/styles.css in your template folder.

You can use your new template for rendering like this:

```
go run gocvgen.go -template your_template_name -yaml path/to/your/cv.yaml
```

If you write your template styles in LESS, a handy tool for compiling it to CSS can be found at https://www.npmjs.com/package/less-watch-compiler and running

```
less-watch-compiler . ../css styles.less
```

will generate a style.css file under the css/ folder and watch for changes in the LESS source files.

### Starting the API server

Start the server by running

```
go run . -server
```

Server is responding in port 8080 by default. You can override the default port by providing an environment variable PORT.

### Running with Web UI and PDF service locally

- Web UI - https://bitbucket.org/rakettitiede/cvgen-web-app
- PDF service - https://bitbucket.org/rakettitiede/cvgen-pdf-render

Start both, Web UI and PDF service locally according to their instructions and start the API server. You should see the Web UI in http://localhost:3000

### Deployment

Everytime a push is made to repository master branch a bitbucket pipeline is launched. This builds, runs tests and deploys the test version to Google Cloud App Engine. Test version of the service can be accessed at https://test-dot-cv-generator-288908.ew.r.appspot.com

To deploy to production you need to create a tag and push it version control:

```
git tag -a vx.y.z -m "Version x.y.z"
git push origin vx.y.z
```

This will launch a bitbucket pipeline and deploy the version to the test environment. Once the functionality has been tested you can promote the Test environment deployment to production at https://cv-generator-288908.ew.r.appspot.com

### Using docker image

#### Build

Build happens inside the container so golang installation is not required.

```
docker build -f Dockerfile -t rakettitiede/gocvgen .
```

Or if you just prefer to build the binary, why not:

```
sudo docker run -it -v $(pwd):/blargh --workdir=/blargh golang:alpine /bin/sh -c "apk add --no-cache git && go get -d -v && go build gocvgen.go"
```

#### Run example

You should replace "PATH_TO_RESUMES_REPO" woth a path to actual resumes repo and ROCKET_SCIENTIST with directory to your resumes yaml file.

```
docker run -v ~/PATH_TO_RESUMES_REPO/resumes:/resumes -t rakettitiede/gocvgen -grade 3 -sales iida -yaml /resumes/ROCKET_SCIENTIST/cv.yaml > ~/Desktop/CV_Generated_example.html
```

### TO-DO

- [] Rewrite CSS. Current one is a mess.
- [] Add web/api endpoint.
- [] As of 2.1.2019 the new look is 90% ready, it should be finished and tested with multiple CVs.
