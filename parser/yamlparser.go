package parser

import (
	"bytes"
	"fmt"
	"html/template"

	"rakettitiede.com/gocvgen/config"
	pdfclient "rakettitiede.com/gocvgen/pdf_client"

	"rakettitiede.com/gocvgen/helpers"
	"rakettitiede.com/gocvgen/models"
)

type YamlParser struct {
	Tpl                  *template.Template
	Grade                int
	TemplateDir          string
	SalesTeamYaml        string
	SalesRep             string
	DefaultSales         string
	IncludeSocialLinks   bool
	TemplateName         string
	Portrait             []byte
	SkillDescriptionYaml string
}

// GenerateHTMLOutput generates HTML from given YAML
func (p YamlParser) GenerateHTMLOutput(yaml []byte) []byte {
	ptrResume := p.parseYamlResume(yaml)

	// Temporary variable to store template buffer
	var ioBuffer bytes.Buffer
	tplErr := p.Tpl.ExecuteTemplate(&ioBuffer, "main.gohtml", ptrResume)

	if tplErr != nil {
		fmt.Println(tplErr)
		panic(tplErr)
	}

	return []byte(ioBuffer.String())
}

// GeneratePDFOutputHeadless generates PDF from given YAML
func (p YamlParser) GeneratePDFOutputHeadless(yaml []byte) []byte {
	htmlContent := p.GenerateHTMLOutput(yaml)
	var htmlHeader, htmlFooter bytes.Buffer
	var i interface{}
	var tplErr error

	logoEncoded := helpers.Base64EncodeFile(p.TemplateDir + "/raketti-logo-orange.svg")
	footerData := models.Footer{Logo: logoEncoded}

	// header and footer templates are optional
	p.executeOptionalTemplate("print-header.gohtml", &htmlHeader, i)
	p.executeOptionalTemplate("print-footer.gohtml", &htmlFooter, footerData)

	if tplErr != nil {
		fmt.Println(tplErr)
		panic(tplErr)
	}

	pdfClient := pdfclient.PDFClient{
		IAPAudience: config.GetEnvVar(config.ENV_VAR_IAP_AUDIENCE),
		ServiceURL:  config.GetEnvVar(config.ENV_VAR_PDF_SERVICE_URL),
	}

	buf := pdfClient.GetPDF(htmlContent, htmlHeader.Bytes(), htmlFooter.Bytes())

	return buf.Bytes()
}

func (p YamlParser) parseYamlResume(yaml []byte) *models.Resume {

	// read static logo and css
	logoEncoded := helpers.Base64EncodeFile(p.TemplateDir + "/RAKETTITIEDE_vektori_simplified.svg")
	css := helpers.ReadFile(p.TemplateDir + "/" + p.TemplateName + "/styles/css/styles.css")

	// encode portrait image
	portraitEncoded := helpers.Base64Encode(p.Portrait)

	// parse static yaml
	salesContact := p.parseSalesContact()
	skillDescriptions := p.parseSkillDescriptions()

	return ProcessResume(yaml, &salesContact, &skillDescriptions, p.Grade, p.IncludeSocialLinks, logoEncoded, portraitEncoded, template.CSS(css))
}

func (p YamlParser) executeOptionalTemplate(templateName string, buffer *bytes.Buffer, data interface{}) {
	template := p.Tpl.Lookup(templateName)
	if template != nil {
		err := template.Execute(buffer, data)

		if err != nil {
			fmt.Printf("Something went wrong while executing template %v: %v", templateName, err)
			panic(err)
		}
	}
}

func (p YamlParser) parseSalesContact() models.SalesContact {
	salesYaml := helpers.ReadFile(p.SalesTeamYaml)
	salesTeam := ProcessSales(salesYaml)
	salesContact, exists := salesTeam[p.SalesRep]

	if !exists {
		salesContact = salesTeam[p.DefaultSales]
	}

	return salesContact
}

func (p YamlParser) parseSkillDescriptions() []models.SkillDescription {
	skillDescriptionYaml := helpers.ReadFile(p.SkillDescriptionYaml)
	skillDescriptions := ProcessSkillDescriptions(skillDescriptionYaml)

	return skillDescriptions
}
