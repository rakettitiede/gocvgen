package parser

import (
	"fmt"
	"html/template"
	"time"

	"gopkg.in/yaml.v2"
	"rakettitiede.com/gocvgen/models"
)

// ProcessSales parses YAML formatted sales team info to a SalesTeam structure
func ProcessSales(bs []byte) models.SalesTeam {
	a := models.SalesTeam{}

	yErr := yaml.Unmarshal(bs, &a)
	if yErr != nil {
		fmt.Println(yErr)
		panic(yErr)
	}

	return a
}

// ProcessSkillDescriptions parses skill description YAML to an array of SkillDescriptions
func ProcessSkillDescriptions(bs []byte) []models.SkillDescription {
	var a []models.SkillDescription

	yErr := yaml.Unmarshal(bs, &a)
	if yErr != nil {
		fmt.Println(yErr)
		panic(yErr)
	}

	return a
}

func ParseResume(bs []byte) (models.Document, error) {
	a := models.Document{}
	err := yaml.Unmarshal(bs, &a)

	return a, err
}

// ProcessResume parses YAML formatted CV to a Resume structure
func ProcessResume(bs []byte, salesContact *models.SalesContact, skillDescriptions *[]models.SkillDescription, grade int, includeSocialLinks bool, logo string, portrait string, css template.CSS) *models.Resume {
	a, err := ParseResume(bs)

	if err != nil {
		panic(err)
	}

	meta := models.Meta{
		CreationTime:       time.Now().Unix(),
		ExpirationTime:     time.Now().AddDate(0, 6, 0).Unix(),
		GradeLimit:         grade,
		IncludeSocialLinks: includeSocialLinks,
		Logo:               logo,
		Portrait:           portrait,
		CSS:                css}

	a.Resume.Meta = meta
	a.Resume.SalesContact = *salesContact
	a.Resume.SkillDescriptions = *skillDescriptions

	return &a.Resume
}
